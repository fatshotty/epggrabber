const Request = require('request-promise');
const Moment = require('moment');
const Bulk = require('batch-promise');

const Event = require('./event');

const SKY_DOMAIN = 'http://guidatv.sky.it';
const CHANNEL_PATH = `${SKY_DOMAIN}/guidatv/canale/{name}.shtml`;

const SINGLE_CHANNEL = "http://guidatv.sky.it/app/guidatv/contenuti/data/grid/{date}/ch_{channel}.js";
class Channel {


  get Id() {
    return this.data.id;
  }
  get Name() {
    return this.data.name;
  }
  get Number() {
    return this.data.number;
  }
  get Service() {
    return this.data.service;
  }
  get Logo() {
    return this.data.logo;
  }
  get Url() {
    let name = this.Name.replace(/ /g,"-").toLowerCase();
    return CHANNEL_PATH.replace('{name}', name);
  }

  get Epg() {
    return this._epg;
  }

  constructor(data) {
    this.data = Object.assign({}, data);
    this._epg = {};
  }


  loadEvents(date) {
    const date_str = Moment(date).format('YY_MM_DD');

    const req = this.request( SINGLE_CHANNEL.replace('{date}', date_str).replace('{channel}', this.data.id) );

    const epg = this._epg[ date.getTime() ] = [];

    console.log('Loading events for', this.data.name);

    return req.then( ( programs ) => {

      let usedate = new Date(date);

      const plans = programs.plan;
      for( let plan of plans ) {
        if ( plan.id == '-1' || plan.id == '1' || plan.id == '0'){
          continue;
        }

        const evt = new Event(plan);

        evt.calculateStartTime(usedate);
        usedate = evt.Start;

        epg.push( evt );
      }

      console.log('Loaded event for', this.data.name);

    }).catch( () => {
      console.error('Error loading channel', this.data.name, date_str);
    });
  }


  loadEventsDetail(date, bulk) {
    console.log('Loading Channel event details', this.data.name);
    const epg = this._epg[ date.getTime() ] || [];
    const events_req = [];
    for( let event of epg ) {
      events_req.push( (res, rej) => {
        event.loadDetails().then(res, rej);
      });
    }

    return Bulk( events_req, bulk || 1);
  }


  request(url) {
    return Request({
      uri: url,
      json: true
    });
  }




}
module.exports = Channel;
