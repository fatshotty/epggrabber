const Moment = require('moment');
const Request = require('request-promise');

const Bulk = require('batch-promise');

const Channel = require('./channel');

const SKY_DOMAIN = 'http://guidatv.sky.it';
const CHANNEL_PATH = `${SKY_DOMAIN}/guidatv/canale/{name}.shtml`;

const URL_CHANNELS = "http://guidatv.sky.it/app/guidatv/contenuti/data/grid/grid_{category}_channels.js";

const CATEGORY = [
  "musica",
  "bambini",
  "news",
  "mondi",
  "cinema",
  "sport",
  "intrattenimento",
  "digitale",
  "primafila",
  "meglio",
];



const SCRAP_LINK = [];
for ( let cat of CATEGORY ) {
  SCRAP_LINK.push( URL_CHANNELS.replace('{category}', cat) );
}

class SkyEpg {

  constructor() {
    this._channels = [];
  }

  get EPG() {
    return this._channels;
  }

  loadChannels(date, bulk) {

    const ps = [];
    for ( let link of SCRAP_LINK ) {
      ps.push( (resolve, reject) => {
        this.request(link).then(resolve, reject);
      } );
    }

    return Bulk( ps, bulk || 1 ).then( (all_channels_sky) => {
      // const all_channels_sky = chls.concat( pf ).concat( dig );
      for ( let res of all_channels_sky ) {
        for( let CHL of res ) {
          const channel_data = {
            id: CHL.id,
            name: CHL.name,
            number: CHL.number,
            service: CHL.service,
            logo: CHL.channelvisore || CHL.channellogonew
          };

          const exists = this.checkExistingChannel( CHL.id );

          if ( !exists ) {
            this._channels.push( new Channel(channel_data) );
          }
        }
      }
    });


  }

  checkExistingChannel(id) {
    const c = this._channels.filter( (c) => {
      return c.Id == id;
    });
    return !!(c && c.length);
  }

  scrapeEpg(date, bulk) {
    console.log('Scraping...');

    return new Promise( (resolve, reject) => {

      const all_channel_req = [];
      for( let chl of this._channels ) {
        all_channel_req.push( (res, rej) => {
          chl.loadEvents(date).then( res, rej );
        });
      }

      Bulk( all_channel_req, bulk || 1).then( () => {

        const all_events_req = [];
        for( let chl of this._channels ) {
          all_events_req.push( (res, rej) => {
            chl.loadEventsDetail(date, bulk).then( res, rej );
          });
        }

        Bulk( all_events_req, bulk || 1).then( resolve, reject );

      });
    });
  }

  getEPGDate(date, starttime) {
    const d = new Date(date);
    const time = starttime.split(':');
    const h = time[0];
    const m = time[1];

    d.setHours( parseInt(h, 10) );
    d.setMinutes( parseInt(m, 10) );

    return d;
  }


  fixEpg() {

    for( let CHL of this._channels ) {
      let name = CHL.name.replace(/ /g,"-").toLowerCase();
      CHL.url = CHANNEL_PATH.replace('{name}', name);
    }

    const channels = Object.keys( this.EPG.epg );
    for( let chl_id of channels ) {
      const CHL = this.EPG.epg[ chl_id ];
      const dates = Object.keys( CHL );
      for ( let datetime_str of dates ) {

        const datetime = Number(datetime_str);
        const day = new Date(datetime);
        let usedate = new Date(day);

        const programs = CHL[ datetime_str ];

        for( let i = programs.length - 1, PRG; PRG = programs[i]; i-- ) {

          if ( PRG.id == '-1' || PRG.id == '1' || PRG.id == '0' ) {
            // skip invalid EPG
            programs.splice( i, 1 );
            continue;
          }

          // FIX: dates
          PRG.start = this.getEPGDate(usedate, PRG.starttime);
          const enddate = new Date( PRG.start );
          enddate.setMinutes( enddate.getMinutes() + parseInt(PRG.dur, 10) );
          PRG.end = enddate;

          usedate = PRG.start;

          // FIX: description
          if ( !PRG.description ) {
            PRG.description = PRG.desc;
          }

          // extract Season/Episode
          const match = PRG.description.match( REG_EXP_SEASON_EPISODE );
          if ( match && match.length && match[0]) {
            const episode = match[0];
            PRG.episode = String.prototype.trim.call(episode);
          }


          // fix poster:
          let poster = PRG.thumbnail_url || '';
          if ( poster == '#' ){
            poster = '';
          }
          if ( poster && poster.indexOf('http') < 0 ) {
            poster = PROGRAM_POSTER.replace('{icon}', PRG.thumbnail_url );
          }
          PRG.poster = poster;

        }

      }
    }
  }


  request(url) {
    console.log('request to', url);
    return Request({
      uri: url,
      json: true
    }).then( (data) => {
      return data;
    }, (err) => {
      console.error(err)
    });
  }


}


module.exports = SkyEpg;
