const SINGLE_EVENT = "http://guidatv.sky.it/EpgBackend/event_description.do?eid={event}"
const Request = require('request-promise');

const SKY_DOMAIN = 'http://guidatv.sky.it';
const PROGRAM_POSTER = `${SKY_DOMAIN}/app/guidatv/images`;
const REG_EXP_SEASON_EPISODE = /^((S(\w+)?(\d+))?)(\s?)((E(\w+)?(\d+))?)/i;

function getEPGDate(date, starttime) {
  const d = new Date(date);
  const time = starttime.split(':');
  const h = time[0];
  const m = time[1];

  d.setHours( parseInt(h, 10) );
  d.setMinutes( parseInt(m, 10) );

  return d;
}


class Event {

  get Start() {
    return this._start
  }
  get Stop() {
    const enddate = new Date( this._start );
    enddate.setMinutes( enddate.getMinutes() + parseInt(this.data.dur, 10) );
    return enddate;
  }
  get Id() {
    return this.data.id
  }
  get Pid() {
    return this.data.pid
  }
  get Title() {
    return this.data.title
  }
  get Genre() {
    return this.data.genre
  }
  get Subgenre() {
    return this.data.subgenre
  }
  get Poster() {
    let poster = this.data.thumbnail_url || '';
    if ( poster == '#' ){
      poster = '';
    }
    if ( poster && poster.indexOf('http') < 0 ) {
      poster = PROGRAM_POSTER.replace('{icon}', this.data.thumbnail_url );
    }
    return poster;
  }
  get Description() {
    return this.data.description || this.data.desc;
  }
  get Episode() {
    const match = this.Description.match( REG_EXP_SEASON_EPISODE );
    if ( match && match.length && match[0]) {
      const episode = match[0];
      return String.prototype.trim.call(episode);
    }
    return ''
  }

  constructor(data) {
    this.data = Object.assign({}, data);
  }

  calculateStartTime(refdate) {
    this._start = getEPGDate(refdate, this.data.starttime);
  }

  loadDetails() {

    console.log('Loading event details for', this.data.id, this.data.desc);
    const req = this.request( SINGLE_EVENT.replace('{event}', this.data.id) );

    return req.then( (event_detail) => {
      console.log('Loaded event details for', this.data.id, this.data.desc);
      if ( !event_detail || !event_detail.description ) {
        console.log('*** no description for', this.data.id, event_detail);
      }
      Object.assign(this.data, event_detail || {});
    });
  }


  request(url) {
    return Request({
      uri: url,
      json: true
    });
  }

}


module.exports = Event;
