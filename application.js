const Request = require('request');
const FS = require('fs');
const Moment = require('moment');
const Pretty = require('pretty-data').pd;
const Net = require('net');

const XMLWriter = require('xml-writer');

const Args = require('yargs');

Args.usage('Usage: $0 -d [num] -s [num] -b [server] -y');
Args.boolean('y').default('y', false);
Args.number('r').default('r', 3).describe('r', 'Number of concurrent requests');
Args.number('d').default('d', 0).describe('d', 'Number of days to load');
Args.number('s').default('s', 0).describe('s', 'Shift hours');
Args.option('sock').describe('sock', 'Specify the sock file which write data onto');
Args.default('o', './guide.xml').describe('o', 'Specify the output file name');
// Args.array('b').default('b', 'sky').describe('b', 'Specify backend server. Available backend: \'sky\'' );

const ArgV = Args.argv;

const today = new Date();
today.setHours(0,0,0,0);

const interval = []; //[ yesterday, today, tomorrow, tomorrow_after ];
let days = ArgV.d + 1;
while( days-- > 0 ) {
  const d = new Date( today );
  d.setDate( d.getDate() + days );
  interval.unshift( d );
}

if ( ArgV.y ) {
  const d = new Date( today );
  d.setDate( d.getDate() - 1 );
  interval.unshift( d );
}

const SHIFT = [];
let _sh = ArgV.s;
while( _sh >= 0 ) {
  SHIFT.unshift( _sh-- );
}

const SkyEpg = require('./sky/sky');
const Sky = new SkyEpg();
const bulk = 3;

function loadByDate(index) {
  const date = interval[ index++ ];
  if ( date ) {
    Sky.loadChannels(date, bulk).then( () => {
      const onFinally = () => {
        loadByDate(index);
      };
      Sky.scrapeEpg(date, bulk).then( onFinally, onFinally );
    });
  } else {
    FS.writeFileSync( './epg.json', JSON.stringify(Sky.EPG, null ,2), {encoding: 'utf-8'} );
    createXML( Sky.EPG );
  }

}
loadByDate(0);

function createXML(EPG) {

  const XW = new XMLWriter();
  XW.startDocument('1.0', 'UTF-8');
  const TV = XW.startElement('tv');
  TV.writeAttribute('source-info-name', 'EPG');
  TV.writeAttribute('generator-info-name', 'simple tv grab it');
  TV.writeAttribute('generator-info-url', '');
  for( let CHL of EPG ) {
    for ( let shift of SHIFT ) {
      const chl_id = shift ? `${CHL.Id}-${shift}` : CHL.Id;

      const chl_name = shift ? `${CHL.Name} +${shift}` : CHL.Name;
      const chl_el = TV.startElement('channel');
      chl_el.writeAttribute('id', chl_id);
      chl_el.writeAttribute('name', chl_name);
      if ( ! shift ) {
        chl_el.writeAttribute('number', CHL.Number);
      }

      chl_el.startElement('display-name')
        .writeAttribute('lang', 'it')
        .text(chl_name)
        .endElement();

      if ( !shift ) {
        chl_el.startElement('display-name')
          .writeAttribute('lang', 'it')
          .text(CHL.Number)
          .endElement();
      }

      chl_el.startElement('icon').writeAttribute('src', CHL.Logo).endElement();
      if ( CHL.Url ) {
        chl_el.startElement('url').text( CHL.Url ).endElement();
      }
      chl_el.endElement();
    }
  }


  for( let CHL of EPG ) {

    for( let shift of SHIFT ) {
      const chl_id = shift ? `${CHL.Id}-${shift}` : CHL.Id;

      const dates = Object.keys( CHL.Epg );

      for ( let datetime_str of dates ) {
        const programs = CHL.Epg[ datetime_str ];

        for ( let PRG of programs ) {

          const prg_el = TV.startElement('programme');

          let starttime = new Date(PRG.Start);
          starttime.setMinutes( starttime.getMinutes() + (60 * shift) );
          prg_el.writeAttribute('start', Moment(starttime).format('YYYYMMDDHHmmss Z').replace(':', '') );

          let endtime = new Date(PRG.Stop);
          endtime.setMinutes( endtime.getMinutes() + (60 * shift) );
          prg_el.writeAttribute('stop', Moment(endtime).format('YYYYMMDDHHmmss Z').replace(':', '') );

          prg_el.writeAttribute('channel', chl_id);

          const id_el = prg_el.startElement('id')
                  .text(PRG.Id)
                  .endElement();
          const pid_el = prg_el.startElement('pid')
                  .text(PRG.Pid)
                  .endElement();
          const prg_title = PRG.Title;
          if ( PRG.prima ) {
            prg_title += ' 1^TV';
          }
          const title_el = prg_el.startElement('title').writeAttribute('lang', 'it')
                  .text(prg_title)
                  .endElement();
          const genre_el = prg_el.startElement('category').writeAttribute('lang', 'it')
                  .text(PRG.Genre)
                  .endElement();
          const subgenre_el = prg_el.startElement('category').writeAttribute('lang', 'it')
                  .text(PRG.Subgenre)
                  .endElement();
          if ( PRG.Poster ) {
            const thumbnail_url_el = prg_el.startElement('icon')
                    .text(PRG.Poster)
                    .endElement();
          }
          const description_el = prg_el.startElement('desc').writeAttribute('lang', 'it')
                  .text(PRG.Description)
                  .endElement();
          const country_el = prg_el.startElement('country')
                  .text('IT')
                  .endElement();
          const subtitles_el = prg_el.startElement('sub-title').writeAttribute('lang', 'it')
                  .text( PRG.data.desc )
                  .endElement();
          const credits_el = prg_el.startElement('credits')
                  .endElement();


          if ( PRG.Episode ) {
            prg_el.startElement('episode-num')
                  .writeAttribute('system', 'onscreen')
                  .text( PRG.Episode )
                  .endElement();
          }

          prg_el.endElement();
        }
      }

    }

  }

  TV.endElement();
  XW.endDocument();

  if ( ArgV.o ) {
    FS.writeFileSync( ArgV.o, Pretty.xml( XW.toString() ), {encoding: 'utf-8'});
  }

  if ( ArgV.sock ) {
    const Client = Net.connect( {path: ArgV.sock}, function () {
      Client.write( XW.toString() );
      Client.end();
      Client.unref();
    });
  }

  console.log('DONE!');
}
